# frozen_string_literal: true

# Task serializer
class TaskSerializer < ActiveModel::Serializer
  attributes :id, :description, :user_id, :username, :created_at, :updated_at, :completed_at, :avatar_url

  def username
    object.user.username
  end
end
