# frozen_string_literal: true

class Task < ApplicationRecord
  # associations
  belongs_to :user
  has_one_attached :avatar

  # validations
  validates :description, presence: true
  validates :avatar,
            allow_blank: true,
            format: { with: URI::DEFAULT_PARSER.make_regexp }

  # scopes
  scope :order_by_completed_at, -> { order('completed_at DESC, created_at DESC') }
end
