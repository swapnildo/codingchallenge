# frozen_string_literal: true

# Main controller
class ApplicationController < ActionController::Base
  include JsonResponses
end
