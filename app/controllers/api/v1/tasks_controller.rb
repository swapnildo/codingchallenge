# frozen_string_literal: true

module Api
  module V1
    # Task's actions
    class TasksController < ApplicationController
      before_action :authenticate_user!

      # All tasks
      def index
        @tasks = TaskService.list(current_user)
        respond_to do |format|
          format.json { render_data(@tasks) }
        end
      end

      # Create task
      def create
        @task = TaskService.create_task(task_params, current_user)
        respond_to do |format|
          format.json do
            if @task.persisted?
              render_success_response
            else
              render_unprocessable_entity_response(@task.errors.full_messages.join(', '))
            end
          end
        end
      end

      # Mark task as completed
      def mark_complete
        task = Task.find_by(id: params[:task_id])
        if task.present? && task.update(completed_at: Time.zone.now)
          render_data({ completed_at: task.completed_at&.to_i })
        else
          render_unprocessable_entity_response(task.errors.full_messages.to_sentence)
        end
      end

      private

      def task_params
        params.require(:task).permit(:description, :avatar_url)
      end
    end
  end
end
