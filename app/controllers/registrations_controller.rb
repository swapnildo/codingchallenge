# frozen_string_literal: true

# User registration controller
class RegistrationsController < Devise::RegistrationsController
  private

  # Override methode to add username
  def sign_up_params
    params.require(:user).permit(:username, :email, :password, :password_confirmation)
  end
end
