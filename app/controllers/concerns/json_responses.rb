# frozen_string_literal: true

# JSON response
module JsonResponses
  extend ActiveSupport::Concern

  private

  def render_data(data)
    render json: data, status: 200
  end

  def render_unprocessable_entity_response(message)
    render json: { status: :unprocessable_entity, error: message }
  end

  def render_success_response
    render json: { status: :created }
  end
end
