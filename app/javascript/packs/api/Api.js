import axios from 'axios'

export const getTasks = async () => {
    let result = await axios.get("/api/v1/tasks");
    return result.data;
}

export const updateTask = async(index) => {
    let csrfToken = document.querySelector("meta[name='csrf-token']").getAttribute("content");
    let config = {
        method: 'post',
        url: '/api/v1/tasks/'+index+'/mark_complete',
        headers: { 
            'Content-Type': 'application/json',
            'X-CSRF-Token': csrfToken 
        }
    }
    let result = await axios(config);
    let data = await axios.get("/api/v1/tasks");
    return data.data;
}

export const createTask = async(task) => {
    let csrfToken = document.querySelector("meta[name='csrf-token']").getAttribute("content");
    
    let taskData = {
        task: task,
    }
    
    let config = {
        method: 'post',
        url: '/api/v1/tasks',
        headers: { 
            'Content-Type': 'application/json',
            'X-CSRF-Token': csrfToken 
        },
        data: taskData
    }
    let result = await axios(config);
    if(result.data.status==="unprocessable_entity") alert("Invalid Avatar URL");
    let data = await axios.get("/api/v1/tasks");
    return data.data;
}
