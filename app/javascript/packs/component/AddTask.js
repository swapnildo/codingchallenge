import React from "react";
import styles from "./AddTask.module.css";
const AddTask = ({ addTask }) => {
  let task = React.createRef();
  let profileImagePath = React.createRef();

  const addToTaskList = () => {
    let url='';
    if(task.current.value!=='')
      {
        if(profileImagePath.current.value!=='')
        {
          url = profileImagePath.current.value.match(/(http(s?):)([/|.|\w|\s|-])*\.(?:jpg|gif|png)$/g);
          if(url === null)
          {
            profileImagePath.current.value = ""
            alert("Invalid image url.");
            return ;
          }
        }
        const taskData = {};
        taskData.description = task.current.value;
        taskData.avatar_url = profileImagePath.current.value;
        addTask(taskData);
      }
    else
      {
        alert("Task field is required");
      }
  }
  return (
    
      <div className={styles.formContainer}>
        <div className={styles.formField}> Task Description</div>
        <input
          type="text"
          className={styles.inputText}
          ref={task}
        ></input>
        <div className={styles.formField}>Avatar URL</div>
        <input
          type="text"
          className={styles.inputText}
          ref={profileImagePath}
        ></input>
        <div className={styles.buttonContainer}>
          <button className={styles.addButton} onClick={() => addToTaskList()}>
            Add
          </button>
        </div>
      </div>
    
  );
};
export default AddTask;
