import React from 'react';
import styles from './TaskDetail.module.css';
import avtar from './profile.png';

import TimeAgo from 'javascript-time-ago';
import en from 'javascript-time-ago/locale/en';
TimeAgo.addDefaultLocale(en);

import ReactTimeAgo from 'react-time-ago';

const TaskDetail = ({ isDone, done, task ,time, taskId, url, username}) => 
{
  return (
    <div className={styles.allTasklist}>
      <div className={styles.taskHeading}>
        <div className={styles.taskHeading}>
        <img src={url || avtar} className={styles.profileImage}/>
        </div>
        <div className={styles.taskListSection}>
          <div class="d-block">{username}</div>
          <small class="d-block mt-1 text-muted">{task}</small>
          </div>
        {done?
        <span className={styles.listSpan}>
          <ReactTimeAgo date={new Date(time)} locale="en-US"/>
        </span>
        :
        <div className={styles.checkBoxContainer}>
          <input
            type="checkbox"
            className={styles.checkBox}
            onChange={() => isDone(taskId)}
          />
        </div>
        }
      </div>
    </div>
  );
}
export default TaskDetail;

