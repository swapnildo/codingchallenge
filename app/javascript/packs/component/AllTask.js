import React from 'react';
import TaskDetail from './TaskDetail';
import * as styles from './AllTask.module.css';
const AllTask = ({ tasks, isDone}) => {
  if(tasks.length == 0) {
    return ( <div className={styles.noData} > No data found please add new tasks.</div>);
  } else {
    return (
      <div className={styles.container} id="task-list">
        <div className={styles.flexContainer}>
          {tasks.map((task,index) => (
            <TaskDetail
            task={task.description}
            username={task.username}
            url ={task.avatar_url}
            time={task.completed_at}
            taskId={task.id}
            key={task.id}
            done = {task.completed_at!==null}
            isDone={isDone}
            />
          ))}
        </div>
      </div>    
  );}
};

export default AllTask;
