import React, { useState, useEffect } from 'react';
import Style from './Task.module.css';
import AllTask from './AllTask';
import AddTask from './AddTask';
import avtar from './back_icon.png';
import {createTask,getTasks,updateTask } from '../api/Api'

const Task = () => {
  const [isTaskOpen, setIsTaskOpen] = useState(true);
  const [taskList,setTaskList] = useState([]);
  const openAddTask = () => {
    setIsTaskOpen(false); 
  };
  
  useEffect(()=>{
    async function getData() {
      const data= await getTasks();
      console.log(data);
      setTaskList(data);
    }
    getData();
  },[]);
  
  const addTask = async (task) =>{
    setIsTaskOpen(true);
    const data = await createTask(task);
    setTaskList(data);
    
  }
  
  const isDone = async(index) => {
    const data = await updateTask(index);
    setTaskList(data);
  }
  
  return  (
    <div className={Style.rootWrapper}>
      {isTaskOpen ? <div className={Style.pageHeader}>
        <div className={Style.taskDescription}>
        Tasks
        </div>
        <div class="d-flex align-items-center">
          <div type="button" onClick={openAddTask} className={Style.addTask}>
            +
          </div>
          <div className={Style.logoutSwitch}><a rel="nofollow" data-method="delete" href="/users/sign_out">Logout</a></div>
        </div>
      </div>
      :
      <div className={Style.pageHeader}>
        <div className={Style.taskDescription}>
        AddTask
        </div>
        <div class="d-flex align-items-center">
          <div type="button" onClick={() => {setIsTaskOpen(true)}} className={Style.addTask}>
            <img src={avtar}/>
          </div>
          <div className={Style.logoutSwitch}>
            <a rel="nofollow" data-method="delete" href="/users/sign_out">Logout</a>
          </div>
        </div>
        
      </div>}
      <div className={Style.container}>
        {isTaskOpen ?  
        <AllTask tasks={taskList} isDone={isDone}/>
          :
          <AddTask addTask={addTask}/>
        }
        </div>
    </div>
  ) 
};

export default Task;
