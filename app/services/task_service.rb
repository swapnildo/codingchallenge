# frozen_string_literal: true

require 'open-uri'

# Task service
class TaskService
  # List all tasks
  def self.list(user)
    user.tasks.order_by_completed_at
  end

  # Task create
  def self.create_task(task_params, user)
    task = user.tasks.new(task_params)
    if task.avatar_url.present?
      unless image_exists?(task.avatar_url)
        task.errors.add(:avatar_url, I18n.t('task.errors.image_url'))
        return task
      end

      task = add_file(task)
    end
    task.save
    task
  rescue StandardError => e
    task.errors.add(:exception, e.message)
    task
  end

  # Check image exists
  def self.image_exists?(url)
    ParserUrl.file_exists?(url)
  rescue Errors::InvalidUrl
    false
  end

  # Add avatar url to task
  def self.add_file(task)
    uri = URI.parse(task.avatar_url)
    file_name = File.basename(uri.path)
    file = uri.open
    task.avatar.attach(io: file, filename: file_name)
    task
  end

  private_class_method :image_exists?, :add_file
end
