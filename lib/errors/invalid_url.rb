# frozen_string_literal: true

module Errors
  class InvalidUrl < StandardError; end
end
