# frozen_string_literal: true

require 'net/http'

# Parse url
class ParserUrl
  def self.file_exists?(url)
    url = URI.parse(url)
    raise Errors::InvalidUrl unless %w[http https].include?(url.scheme)

    http = net_http_initialize(url)
    http.start do |request|
      resp = request.head(url.request_uri)
      raise Errors::InvalidUrl if resp['Content-Type'].nil? || !(resp['Content-Type'].start_with? 'image')
    end
    true
  end

  def self.net_http_initialize(url)
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = (url.scheme == 'https')
    http
  end

  private_class_method :net_http_initialize
end
