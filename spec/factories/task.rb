# frozen_string_literal: true

FactoryBot.define do
  factory :task do
    description { 'Test description' }
    avatar_url { Faker::Internet.url }
    user
  end
end
