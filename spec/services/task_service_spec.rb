# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TaskService do
  before do
    @user = create(:user)
  end

  describe '#list' do
    let(:user) { create(:user) }

    it 'renders all tasks list' do
      tasks = Task.create([{ description: I18n.t('test.description'), avatar_url: '',
                             completed_at: Time.current, user_id: @user.id }])
      expect(TaskService.list(@user)).to eq(tasks)
    end
  end

  describe '#create_task' do
    let(:user) { create(:user) }

    it 'create tasks' do
      task = { description: I18n.t('test.description'), avatar_url: '' }
      expect { TaskService.create_task(task, @user) }.to change(Task, :count).by(1)
    end

    it 'is invalid without description' do
      task_hash = { description: '', avatar_url: '' }
      task = TaskService.create_task(task_hash, @user)
      expect(task.errors.full_messages.join(', ')).to match(I18n.t('test.errors.description'))
    end

    it 'is invalid url' do
      task_hash = { description: I18n.t('test.description'),
                    avatar_url: 'ya-webdesign.com/transparent250_/funny-png-avatar-2.png' }
      task = TaskService.create_task(task_hash, @user)
      expect(task.errors.full_messages.join(', ')).to match(I18n.t('test.errors.image_url'))
    end
  end
end
