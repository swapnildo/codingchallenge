# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::TasksController do
  before do
    @user = create(:user)
  end

  before :each do
    sign_in @user
    request.env['HTTP_ACCEPT'] = 'application/json'
  end

  describe 'GET #index' do
    let(:user) { create(:user) }

    it 'renders all tasks list page' do
      get :index
      expect(response.status).to eq(200)
    end

    it 'assigns tasks' do
      get :index
      expect(response.status).not_to be_nil
    end
  end

  describe 'POST #create' do
    let(:user) { create(:user) }

    it 'creates task' do
      expect do
        post :create, params: { task: { description: I18n.t('test.description'),
                                        avatar_url: I18n.t('test.url'),
                                        user_id: user.id } }
      end.to change(Task, :count).by(1)
    end

    it 'returns valid message with json' do
      post :create, params: { task: { description: I18n.t('test.description'),
                                      avatar_url: I18n.t('test.url'),
                                      user_id: user.id } }
      expect(response.body).to match({ status: 'created' }.to_json)
    end

    it 'checks presence of description with json' do
      post :create, params: { task: { description: '',
                                      avatar_url: I18n.t('test.url'),
                                      user_id: user.id } }
      expect(response.body).to match({ status: 'unprocessable_entity',
                                       error: I18n.t('test.errors.description') }.to_json)
    end
  end

  describe 'POST #mark_complete' do
    let(:task) { create(:task) }

    it 'returns success true message' do
      post :mark_complete, params: { task_id: task.id }, format: :json
      parsed_body = JSON.parse(response.body)
      # expect(parsed_body['status']).to match('ok')
      expect(parsed_body['completed_at']).to be_within(5).of(Time.zone.now.to_i)
    end

    it 'changes the value of completed_at' do
      expect do
        post :mark_complete, params: { task_id: task.id }, format: :json
      end.to(change { task.reload.completed_at })
    end
  end
end
