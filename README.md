# Coding Challenge Submission
This repo has 2 coding challenges in PDF format(One for Rails and one for Reactjs).   As a candidate, you must pick the challenge for which you are interviewing for.  
Follow the instructions and if they are unclear, please contact the hiring manager. 

Thank you very much for applying at DoubleGDP.

# Important!!
Do not submit a pull request to this repository.  You PR wil be rejected and your submission ignored.
To properly submit a coding challenge you must:

- fork this repository
- make the necessary changes
- push changes to your forked origin
- send address of your fork to the hiring manager.

We will review your fork online before and during your interview.


# Task App

## Dependencies
* Ruby version : 3.0.0
* Rails Version : 6.1.3.1
* Yarn Version : 1.22.5
* Node Version : 12.22.0
* React Version : 17.0.2

## Configuration
```gem install bundler:2.2.15 && bundle install```
```yarn install```

## Setup and Start the Applicaton

### Database Setup
```rake db:create && rake db:migrate && rake db:seed```

### Run the rails server
```rails s```

### Start webpacker
```./bin/webpack-dev-server```

## Access

### Home page
```http://localhost:3000```

## Test Environment Setup

### Test Database Setup
```RAILS_ENV=test rake db:create && RAILS_ENV=test rake db:migrate```

### Run the Test Suit
```rspec```

## Future enhancements
- Add filters to tasks.
- Add pagination.
- Add further CRUD operations for task.
- Error handling can be made more robust.
- Add forgot password functionality.
- Add email functionality.
- Integrate sidekiq gem for background email service.
- Use postgres as database.
- Soft delete records.
- Better user interface.
