# frozen_string_literal: true

# Create task
class CreateTasks < ActiveRecord::Migration[6.1]
  def change
    create_table :tasks do |t|
      t.string :description, null: false
      t.string :avatar_url
      t.datetime :completed_at
      t.references :user, null: false, foreign_key: true
      t.timestamps

      # add index to completed_at
      t.index :completed_at
    end
  end
end
